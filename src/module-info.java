module YourProjectName { 
    requires javafx.swt;
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.swing;
    requires javafx.web;
    requires java.base;
    requires javafx.media;
   
    opens posclock;
}