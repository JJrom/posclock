package posclock;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;


public class PosClock {

    PosClock p = this;

    private long returnTime = 0;
    private long pauseTime = 0;
    private Timer posession = new Timer();
    private Label label;
    boolean paused = false;
    /**
     * 
     * @param team Team that starts attack as String
     * @param targetTime Long for maximum duration of attack
     * @param l Label for countdown timer
     * @param home_pos Label of home, used to show attacking/defending side
     * @param away_pos Label of away, used to show attacking/defending side
     */

    public void timer(String team, long targetTime, Label l, Label home_pos, Label away_pos) {
        posession.cancel();
        PosClock.this.posession = new Timer();
        label = l;
        long startingTime = System.currentTimeMillis();
        DecimalFormat df = new DecimalFormat("##,0.##");
        returnTime = targetTime;
        System.out.println("Palloa hallitsee: " + team);
        if ("Home".equals(team)) {
            home_pos.setTextFill(Color.web("#006400"));// .setTextFill(Color.web("#0076a3"));
            away_pos.setTextFill(Color.web("#ff0000"));
        } else if ("Away".equals(team)) {
            away_pos.setTextFill(Color.web("#006400"));// .setTextFill(Color.web("#0076a3"));
            home_pos.setTextFill(Color.web("#ff0000"));
        }
        posession.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!paused) {

                    long timePassed = ((System.currentTimeMillis() - startingTime));
                    if (returnTime - timePassed > 5000) {
                        System.out.println("Aikaa jäljellä " + (returnTime - timePassed) / 1000);
                        String si = (" " + (returnTime - timePassed) / 1000);
                        Platform.runLater(() -> {
                            l.setText(si);
                        });

                    } else if ((returnTime - timePassed > 0)) {
                        System.out.println("Aikaa Jäljellä " + df.format((returnTime - timePassed) / 100));
                        String si = ((df.format((returnTime - timePassed) / 100)).toString());
                        Platform.runLater(() -> {
                            l.setText(si);
                        });

                    } else {
                        buzzer();
                        System.out.println("Playing buzzer to signal time ending");
                        attackSideChange(team, targetTime, l, home_pos, away_pos);
                        System.out.println("After time ends, changing teams and restarting the clock");
                        pause();
                        System.out.println("Pausing the game");
                    }

                }
            }

        }, 0, 100);

    }

    /**
     * Changes current team, Home and away used as placeholders
     * @param currentTeam
     * @return 
     */
    public String swapTeam(String currentTeam) {
        if ("Home".equals(currentTeam)) {
            return "Away";
        } else {
            return "Home";
        }

    }
    
    /**
     * Used to reset clock and change team
     * @param currentTeam Current team to be changed
     * @param targetTime Long to measure attack time length
     * @param l Label of Clock, used to modify remaining time
     * @param home_pos Label of home, used to show attacking/defending side
     * @param away_pos Label of away, used to show attacking/defending side
     */

    public void attackSideChange(String currentTeam, long targetTime, Label l, Label home_pos, Label away_pos) {
        returnTime = 0;
        posession.cancel();
        PosClock.this.posession = new Timer();
        PosClock.this.timer(swapTeam(currentTeam), 24000, l, home_pos, away_pos);

    }
    
    /**
     * Pauses the game, pauseTime is used to calculate passed time when resuming
     * 
     */

    public void pause() {
        System.out.println("Pause");
        pauseTime = System.currentTimeMillis();
        paused = true;
    }
    
    /**
     * Resumes the game
     * returnTime is used to calculate manually changed time with the following formula
     *  returnTime = returnTime + (System.currentTimeMillis() - pauseTime);
     */

    public void resume() {
        if (paused == true) {
            System.out.println("Resuming");
            if (pauseTime != 0) {
                returnTime = returnTime + (System.currentTimeMillis() - pauseTime);
            }
            paused = false;
        }
    }
    
    /**
     * Changes max attack time, and returns new in long
     * @param currentTime Current max attack time to be changed
     * @return 
     */

    public long changeAttackTime(long currentTime) {
        if (currentTime == 24000) {
            currentTime = 14000;
        } else {
            currentTime = 24000;
        }
        return currentTime;

    }
    
    /**
     * Plays buzzer sound to show ending of time
     * 
     */

    public void buzzer() {
          try {
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(this.getClass().getResource("Buzzer.wav"));
        Clip clip = AudioSystem.getClip();
        clip.open(audioInputStream);
        clip.start();
        // If you want the sound to loop infinitely, then put: clip.loop(Clip.LOOP_CONTINUOUSLY); 
        // If you want to stop the sound, then use clip.stop();
    } catch (Exception ex) {
        ex.printStackTrace();
    }
      
    }
    
    /**
     * Changes time remaining
     * @param time Amount of time to add
     */

    public void changeTime(int time) {
        switch (time) {
            case (1):
                returnTime = returnTime + 1000;
                break;
            case (5):
                returnTime = returnTime + 5000;
                break;

            case (10):
                returnTime = returnTime + 10000;
                break;

            case (-1):
                returnTime = returnTime - 1000;
                break;
            case (-5):
                returnTime = returnTime - 5000;
                break;

            case (-10):
                returnTime = returnTime - 10000;
                break;

        }

    }

   

}
